# enrichmentAnalysisGO

The project aims at demonstrating the principles of enrichment analysis of the set of proteins based on the [Gene Ontology](https://geneontology.org/) annotations.


# Todo

- [ ] write a nextflow workflow for generating and analyzing the data

